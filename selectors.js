[...document.querySelectorAll('.mkwp[id]')].map(item => {
  return {
    "mmk_url": item.querySelector('a').href,
    "mmk_img": item.querySelector("[srcset *= 'mobile']").srcset,
    "mmk_img_width": "320",
    "mmk_img_height": "320",
    "mmk_Headline": item.querySelector('.caption--headline').textContent.trim(),
    "font_family": [...item.querySelector('.caption').classList].join(' '),
    "mmk_subtext": item.querySelector('.caption--headline + div').textContent.trim()
  }
})

var all = [...$0.querySelectorAll('a')].map(item => {
  return {
    "cta_text": item.textContent,
    "cta_url": item.href
  }
})

[...document.querySelector('.carousel').querySelectorAll('figure.promo')].map(item => {
  return {
    "slider_img": item.querySelector("[srcset *= 'mobile']").srcset,
    "slider_img_height": "320",
    "slider_img_width": "320",
    "slider_caption": item.querySelector('.caption--ctas').textContent.trim(),
    "slider_url": item.querySelector('a').href,
    "slider_text": item.querySelector('.wpHeadline').textContent.trim()
  }
})